﻿using BSA.BL.DTO.Task;
using BSA.BL.DTO.User;
using BSA.Repo.Enums;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BSA.WebAPI.IntegrationalTests
{
    public class UserControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private HttpClient _client;
        private const string URL = "https://localhost:44337";

        public UserControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task DeleteUser_WhenUserExist_ThenStatusCodeNoContent()
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 21),
                TeamId = null
            };

            var jsonString = JsonConvert.SerializeObject(user);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var addResponse = await _client.PostAsync(URL + "/api/user", content);
            var addResultJson = await addResponse.Content.ReadAsStringAsync();
            var addedObj = JsonConvert.DeserializeObject<UserDTO>(addResultJson);

            var delResponse = await _client.DeleteAsync(URL + "/api/user/" + addedObj.Id);

            Assert.Equal(HttpStatusCode.NoContent, delResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserDoesNotExistExist_ThenStatusCodeNotFound()
        {
            var delResponse = await _client.DeleteAsync(URL + "/api/user/" + int.MaxValue);

            Assert.Equal(HttpStatusCode.NotFound, delResponse.StatusCode);
        }

        [Fact]
        public async Task GetSortedUsersWithSortedTasks_WhenUserExist_ThenStatusCodeOkAndCollectionUserWithTasksDTO()
        {
            var response = await _client.GetAsync(URL + "/api/user/sorted/tasks/sorted");
            var resultJson = await response.Content.ReadAsStringAsync();
            var objs = JsonConvert.DeserializeObject<List<UserWithTasksDTO>>(resultJson);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotEmpty(objs);

            var expected = objs.OrderBy(u => u.User.FirstName).Select(u =>
            {
                u.Tasks.OrderByDescending(t => t.Name.Length);
                return u;
            }).ToList();

            Assert.True(expected.SequenceEqual(objs));
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].Tasks.SequenceEqual(objs[i].Tasks));
            }
        }

        [Theory]
        [InlineData(4)]
        [InlineData(32)]
        [InlineData(48)]
        public async Task GetNotFinishedTasksOFUser_WhenUserExist_ThenStatusCodeOkAndCollectionTaskDTO(int id)
        {
            var response = await _client.GetAsync(URL + "/api/user/" + id + "/tasks/NotFinished");
            var resultJson = await response.Content.ReadAsStringAsync();
            var objs = JsonConvert.DeserializeObject<List<TaskDTO>>(resultJson);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(objs.All(t => t.State != TaskState.Finished && t.PerformerId == id));
        }

        [Theory]
        [InlineData(400)]
        [InlineData(324737)]
        [InlineData(int.MaxValue)]
        public async Task GetNotFinishedTasksOFUser_WhenUserDoesNotExist_ThenStatusCodeNotFound(int id)
        {
            var response = await _client.GetAsync(URL + "/api/user/" + id + "/tasks/NotFinished");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
