﻿using AutoMapper;
using BSA.BL.DTO.Team;
using BSA.BL.Exceptions;
using BSA.BL.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.UnitOfWorker;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace BSA.BL.Tests
{
    public class TeamServiceTests
    {
        private BsaDbContext _ctx;
        private TeamService _teamService;
        private ProjectService _projService;

        public TeamServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<BsaDbContext>()
                                .UseInMemoryDatabase(databaseName: "Test")
                                .Options;
            _ctx = new BsaDbContext(dbOptions);

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();

            var uow = new UnitOfWorker(_ctx);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            var mapper = new Mapper(mapperConfig);
            _teamService = new TeamService(uow, mapper);
            _projService = new ProjectService(uow, mapper);
        }

        public void Dispose()
        {
            _ctx.Database.EnsureDeleted();
            _ctx.Dispose();
        }

        [Fact]
        public void GetAllTeams_WhenSeedData_ThenCountEqual10()
        {
            var teams = _teamService.GetAllTeams();

            Assert.Equal(10, teams.Count());
        }

        [Theory]
        [InlineData(2)]
        [InlineData(5)]
        [InlineData(7)]
        public void GetTeamsById_WhenSeedDataAndTeamExist_ThenTeamDTOWithId(int id)
        {
            var team = _teamService.GetTeamById(id);

            Assert.NotNull(team);
            Assert.Equal(id, team.Id);
        }

        [Theory]
        [InlineData(20)]
        [InlineData(500)]
        [InlineData(721)]
        public void GetTeamById_WhenSeedDataAndTeamDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _teamService.GetTeamById(id));
        }

        [Fact]
        public void AddTeam_WhenSeeddataAndNewTeamDTOIsValid_ThenReturnTeamDTO()
        {
            var team = new NewTeamDTO
            {
                Name = "Test"
            };

            var addedTeam = _teamService.AddTeam(team);

            Assert.NotNull(addedTeam);
            Assert.Equal(team.Name, addedTeam.Name);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        [InlineData(7)]
        public void UpdateTeam_WhenSeedDataAndValidNewTeamDTOAndTeamExist_ThenTeamDTOWithNewName(int id)
        {
            var prevTeam = _teamService.GetTeamById(id);
            var team = new NewTeamDTO
            {
                Name = "Name"
            };

            _teamService.UpdateTeam(team, id);
            var newTeam = _teamService.GetTeamById(id);

            Assert.NotEqual(prevTeam.Name, newTeam.Name);
        }

        [Theory]
        [InlineData(1004)]
        [InlineData(1404)]
        [InlineData(int.MaxValue)]
        public void UpdateTeam_WhenSeedDataAndValidNewTeamDTOAndTeamDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            var team = new NewTeamDTO
            {
                Name = "Name"
            };

            Assert.Throws<NotFoundException>(() => _teamService.UpdateTeam(team, id));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(7)]
        [InlineData(5)]
        public void DeleteTeam_WhenSeedDataAndTeamExistWithId_ThenNotFoundExeptionOnGetTeamByIdAndCountOfProjectsWithTeamIdEquals0(int id)
        {
            _teamService.DeleteTeam(id);
            var projects = _projService.GetAllProjects().Where(p => p.TeamId == id);

            Assert.Throws<NotFoundException>(() => _teamService.GetTeamById(id));
            Assert.Empty(projects);
        }

        [Theory]
        [InlineData(73285)]
        [InlineData(2351)]
        [InlineData(int.MaxValue)]
        public void DeleteTeam_WhenSeedDataAndTeamDoesNotExist_ThrowNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _teamService.DeleteTeam(id));
        }

        [Fact]
        public void GetTeamsWithUsersOlderThanTenYears_WhenSeedData_ThenCollectionOfTeamWithUsersDTOWhereUsersOlder10()
        {
            var result = _teamService.GetTeamsWithUsersOlderThanTenYears();

            Assert.DoesNotContain(result, r => r.Users.Any(u => CalculateAge(u.Birthday) < 10));
        }

        private int CalculateAge(DateTime dateOfBirth)
        {
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;

            return age;
        }
    }
}
