﻿using AutoMapper;
using BSA.BL.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.BL.Tests.MappingProfiles
{
    public class UserTestMappingProfile : Profile
    {
        public UserTestMappingProfile()
        {
            CreateMap<UserDTO, NewUserDTO>();
        }
    }
}
