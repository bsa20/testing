﻿using AutoMapper;
using BSA.BL.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA.BL.Tests.MappingProfiles
{
    public class TaskTestMappingProfile : Profile
    {
        public TaskTestMappingProfile()
        {
            CreateMap<TaskDTO, NewTaskDTO>();
        }
    }
}
