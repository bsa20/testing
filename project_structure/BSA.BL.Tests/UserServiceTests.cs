﻿using AutoMapper;
using BSA.BL.DTO.Task;
using BSA.BL.DTO.User;
using BSA.BL.Exceptions;
using BSA.BL.MappingProfiles;
using BSA.BL.Services;
using BSA.BL.Tests.MappingProfiles;
using BSA.Repo.Context;
using BSA.Repo.Enums;
using BSA.Repo.UnitOfWorker;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace BSA.BL.Tests
{
    public class UserServiceTests
    {
        private BsaDbContext _ctx;
        private Mapper _mapper;
        private TaskService _taskService;
        private UserService _userService;

        public UserServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<BsaDbContext>()
                                .UseInMemoryDatabase(databaseName: "Test")
                                .Options;
            _ctx = new BsaDbContext(dbOptions);

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();

            var uow = new UnitOfWorker(_ctx);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<UserTestMappingProfile>();
            });
            _mapper = new Mapper(mapperConfig);
            _taskService = new TaskService(uow, _mapper);
            _userService = new UserService(uow, _mapper);
        }

        public void Dispose()
        {
            _ctx.Database.EnsureDeleted();
            _ctx.Dispose();
        }

        [Fact]
        public void GetAllUsers_WhenSeedData_ThenCount50()
        {
            var users = _userService.GetAllUsers();

            Assert.Equal(50, users.Count());
        }

        [Theory]
        [InlineData(7)]
        [InlineData(19)]
        [InlineData(48)]
        public void GetUserByID_WhenSeedDataAndUserExist_ThenUserDTOWithId(int id)
        {
            Assert.Equal(id, _userService.GetUserById(id).Id);
        }

        [Theory]
        [InlineData(700)]
        [InlineData(1048182)]
        [InlineData(int.MaxValue)]
        public void GetUserByID_WhenSeedDataAndUserDoesNotExist_ThenNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _userService.GetUserById(id));
        }

        [Fact]
        public void AddUser_WhenSeedDataAndTeamExist_ThenUserDTO()
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 5),
                TeamId = 3
            };

            var newUser = _userService.AddUser(user);

            Assert.Equal(user.FirstName, newUser.FirstName);
            Assert.Equal(user.LastName, newUser.LastName);
            Assert.Equal(user.Email, newUser.Email);
            Assert.Equal(user.Birthday, newUser.Birthday);
            Assert.Equal(user.TeamId, newUser.TeamId);
        }

        [Fact]
        public void AddUser_WhenSeedDataAndTeamDoesNotExist_ThenThrowNotFoundExeption()
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 5),
                TeamId = 100
            };

            Assert.Throws<NotFoundException>(() => _userService.AddUser(user));
        }

        [Theory]
        [InlineData(5)]
        [InlineData(25)]
        [InlineData(32)]
        public void UpdateUser_WhenSeedDataAndUserExist_ThenUserDTOWithNewDataOnGetUserById(int id)
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 5),
                TeamId = 5
            };

            _userService.UpdateUser(user, id);

            var newUser = _userService.AddUser(user);

            Assert.Equal(user.FirstName, newUser.FirstName);
            Assert.Equal(user.LastName, newUser.LastName);
            Assert.Equal(user.Email, newUser.Email);
            Assert.Equal(user.Birthday, newUser.Birthday);
            Assert.Equal(user.TeamId, newUser.TeamId);
        }

        [Theory]
        [InlineData(5000)]
        [InlineData(5525)]
        [InlineData(int.MaxValue)]
        public void UpdateUser_WhenSeedDataAndUserDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 5),
                TeamId = 5
            };

            Assert.Throws<NotFoundException>(() => _userService.UpdateUser(user, id));
        }

        [Fact]
        public void UpdateUser_AddUserToTeam_WhenSeedDataAndUserExistAndTeamExist_ThenUserDTOOnGetUserByIdWithTeam()
        {
            var user = new NewUserDTO
            {
                FirstName = "First",
                LastName = "Last",
                Email = "First.Last@test.com",
                Birthday = new DateTime(2001, 10, 5),
                TeamId = null
            };

            var oldUser = _userService.AddUser(user);
            var newUserDTO = _mapper.Map<NewUserDTO>(oldUser);
            newUserDTO.TeamId = 5;
            _userService.UpdateUser(newUserDTO, oldUser.Id);
            var newUser = _userService.GetUserById(oldUser.Id);

            Assert.Equal(user.FirstName, newUser.FirstName);
            Assert.Equal(user.LastName, newUser.LastName);
            Assert.Equal(user.Email, newUser.Email);
            Assert.Equal(user.Birthday, newUser.Birthday);
            Assert.Equal(5, newUser.TeamId);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(12)]
        public void DeleteUser_WhenSeedDataAndUserExist_ThenThrowNotFoundExeptionOnGetUserById(int id)
        {
            _userService.DeleteUser(id);

            Assert.Throws<NotFoundException>(() => _userService.GetUserById(id));
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(43)]
        public void GetMainInformationAboutUser_WhenSeedDataAndUserExist_ThenUserMainInfoDTO(int id)
        {
            var userInfo = _userService.GetMainInformationAboutUser(id);

            Assert.NotNull(userInfo.User);
            Assert.Equal(id, userInfo.User.Id);
        }

        [Fact]
        public void GetSortedUsersWithSortedTasks_WhenSeedData_ThenCollectionUserWithTasksDTO()
        {
            var usersWithTasks = _userService.GetSortedUsersWithSortedTasks().ToList();

            var expected = usersWithTasks.OrderBy(u => u.User.FirstName).Select(u =>
            {
                u.Tasks.OrderByDescending(t => t.Name.Length);
                return u;
            }).ToList();

            Assert.True(expected.SequenceEqual(usersWithTasks));
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(expected[i].Tasks.SequenceEqual(usersWithTasks[i].Tasks));
            }
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(43)]
        public void GetCountOfTasksOfUserInProject_WhenSeedDataAndUserExist_ThenDictionareWithCount100(int id)
        {
            var countOftasks = _userService.GetCountOfTasksOfUserInProject(id);

            Assert.Equal(100, countOftasks.Count());
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(43)]
        public void GetTasksOfUserNameLengthLessFourtyFive_WhenSeedDataAndUserExist_ThenCollectionOfTaskDTO(int id)
        {
            var tasks = _userService.GetTasksOfUserNameLengthLessFourtyFive(id);

            Assert.True(tasks.All(t => t.Name.Length < 45));
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(43)]
        public void GetTasksOfUserFinishedThisYear_WhenSeedDataAndUserExist_ThenCollectionShortTaskInfoDTO(int id)
        {
            var tasks = _userService.GetTasksOfUserFinishedThisYear(id);
            var allTasks = _taskService.GetAllTasks();

            var expected = allTasks.Where(t => t.PerformerId == id && t.State == TaskState.Finished && t.FinishedAt.Year == 2020)
                                   .Select(t => new ShortTaskInfoDTO() { Id = t.Id, Name = t.Name }).ToList();

            var intersected = expected.Select(e => e.Id).Intersect(tasks.Select(t => t.Id)).ToList();

            Assert.Equal(tasks.Count(), intersected.Count());
        }

        [Theory]
        [InlineData(5)]
        [InlineData(8)]
        [InlineData(43)]
        public void GetNotFinishedTasksOFUser_WhenSeedDataAndUserExist_ThenCollectionTaskDTOWithTaskStateNotFinished(int id)
        {
            var tasks = _userService.GetNotFinishedTasksOFUser(id);

            Assert.True(tasks.All(t => t.State != TaskState.Finished && t.PerformerId == id));
        }

        [Theory]
        [InlineData(500)]
        [InlineData(889374)]
        [InlineData(int.MaxValue)]
        public void GetNotFinishedTasksOFUser_WhenSeedDataAndUserDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _userService.GetNotFinishedTasksOFUser(id));
        }
    }
}
