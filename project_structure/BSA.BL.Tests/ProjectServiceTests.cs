﻿using AutoMapper;
using BSA.BL.DTO.Project;
using BSA.BL.Exceptions;
using BSA.BL.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.UnitOfWorker;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace BSA.BL.Tests
{
    public class ProjectServiceTests : IDisposable
    {
        private BsaDbContext _ctx;
        private ProjectService _projService;

        public ProjectServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<BsaDbContext>()
                                .UseInMemoryDatabase(databaseName: "Test")
                                .Options;
            _ctx = new BsaDbContext(dbOptions);

            _ctx.Database.EnsureDeleted();
            _ctx.Database.EnsureCreated();

            var uow = new UnitOfWorker(_ctx);

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });
            var mapper = new Mapper(mapperConfig);
            _projService = new ProjectService(uow, mapper);
        }

        public void Dispose()
        {
            _ctx.Database.EnsureDeleted();
            _ctx.Dispose();
        }

        [Fact]
        public void GetAllProjects_WhenSeededData_ThenCount100()
        {
            var projects = _projService.GetAllProjects();

            Assert.Equal(100, projects.Count());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(10)]
        [InlineData(28)]
        public void GetProjectById_WhenSeedData_ThenProjectDTOEntityWithId(int id)
        {
            var project = _projService.GetProjectById(id);

            Assert.NotNull(project);
            Assert.Equal(id, project.Id);
        }

        [Theory]
        [InlineData(500)]
        [InlineData(250)]
        [InlineData(int.MaxValue)]
        public void GetProjectById_WhenSeedDataAndProjectDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _projService.GetProjectById(id));
        }

        [Fact]
        public void AddProject_WhenSeedDataAndNewProject_ThenProjectDTO()
        {
            var project = new NewProjectDTO()
            {
                Name = "Name",
                Description = "Description",
                Deadline = new DateTime(2022, 10, 12),
                AuthorId = 5,
                TeamId = 2
            };

            var newProj = _projService.AddProject(project);

            Assert.Equal(project.Name, newProj.Name);
            Assert.Equal(project.Description, newProj.Description);
            Assert.Equal(project.Deadline, newProj.Deadline);
            Assert.Equal(project.AuthorId, newProj.AuthorId);
            Assert.Equal(project.TeamId, newProj.TeamId);
        }

        [Fact]
        public void AddProject_WhenSeedDataAndAuthorDoesNotExist_ThenThrowNotFoundExeption()
        {
            var project = new NewProjectDTO()
            {
                Name = "Name",
                Description = "Description",
                Deadline = new DateTime(2022, 10, 12),
                AuthorId = 500,
                TeamId = 2
            };

            Assert.Throws<NotFoundException>(() => _projService.AddProject(project));
        }

        [Fact]
        public void AddProject_WhenSeedDataAndTeamDoesNotExist_ThenThrowNotFoundExeption()
        {
            var project = new NewProjectDTO()
            {
                Name = "Name",
                Description = "Description",
                Deadline = new DateTime(2022, 10, 12),
                AuthorId = 5,
                TeamId = 200
            };

            Assert.Throws<NotFoundException>(() => _projService.AddProject(project));
        }

        [Theory]
        [InlineData(2)]
        [InlineData(10)]
        [InlineData(28)]
        public void UpdateProject_WhenSeedDataAndNewProject_ThenProjectDTO(int id)
        {
            var prevProj = _projService.GetProjectById(id);
            var proj = new NewProjectDTO
            {
                Name = "NewName",
                Description = "NewDescription",
                AuthorId = 10,
                TeamId = 5
            };

            _projService.UpdateProject(proj, id);
            var newProj = _projService.GetProjectById(id);

            Assert.Equal(prevProj.Id, newProj.Id);
            Assert.Equal(prevProj.CreatedAt, newProj.CreatedAt);
            Assert.Equal(proj.Name, newProj.Name);
            Assert.Equal(proj.Description, newProj.Description);
            Assert.Equal(proj.AuthorId, newProj.AuthorId);
            Assert.Equal(proj.TeamId, newProj.TeamId);
        }

        [Theory]
        [InlineData(251)]
        [InlineData(155)]
        [InlineData(int.MaxValue)]
        public void UpdateProject_WhenSeddDataAndProjectWithThisIDDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            var proj = new NewProjectDTO
            {
                Name = "NewName",
                Description = "NewDescription",
                AuthorId = 10,
                TeamId = 5
            };

            Assert.Throws<NotFoundException>(() => _projService.UpdateProject(proj, id));
        }

        [Theory]
        [InlineData(5)]
        [InlineData(12)]
        [InlineData(45)]
        public void DeleteProject_WhenSeedDataAndProjectWithIdExist_ThenThrowNotFoundExeptionWhenGetProjectByID(int id)
        {
            _projService.DeleteProject(id);
            Assert.Throws<NotFoundException>(() => _projService.GetProjectById(id));
        }

        [Theory]
        [InlineData(515)]
        [InlineData(1202)]
        [InlineData(int.MaxValue)]
        public void DeleteProject_WhenSeedDataAndProjectDoesNotExist_ThenThrowNotFoundExeption(int id)
        {
            Assert.Throws<NotFoundException>(() => _projService.DeleteProject(id));
        }

        [Fact]
        public void GetMainInfoAboutProjects_WhenSeedData_ThenCountOfProjectMainInfoDTOEqual100AndAllEntitiesNotNull()
        {
            var projects = _projService.GetMainInfoAboutProjects();
            Assert.Equal(100, projects.Count());
            foreach (var proj in projects)
            {
                Assert.NotNull(proj);
            }
        }
    }
}
