﻿using Client.DTO.Task;
using Client.DTO.Team;
using Client.DTO.User;
using System;
using System.Collections.Generic;

namespace Client.DTO.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }

        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }

        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
