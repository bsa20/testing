﻿using System;

namespace Client.DTO.User
{
    public class NewUserDTO
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime Birthday { get; set; }

        public int? TeamId { get; set; }
    }
}
