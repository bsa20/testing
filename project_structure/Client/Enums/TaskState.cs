﻿namespace Client.Enums
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
