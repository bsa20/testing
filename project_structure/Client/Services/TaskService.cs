﻿using Client.API;
using Client.DTO.Task;
using Client.Services.HttpServices.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Services
{
    public class TaskService : HttpService<TaskDTO, NewTaskDTO>
    {
        private readonly ApiOptions _apiOptions;

        public TaskService()
        {
            _apiOptions = new ApiOptions();
        }

        public async Task<IEnumerable<TaskDTO>> GetAll()
        {
            return await this.GetAllEntities(_apiOptions.GetTaskSufix);
        }

        public async Task<TaskDTO> GetById(int id)
        {
            return await this.GetEntityById(_apiOptions.GetTaskSufix, id);
        }

        public async Task<TaskDTO> Create(NewTaskDTO task)
        {
            return await this.PostRequest(_apiOptions.PostTaskSufix, task);
        }

        public async Task<string> Update(NewTaskDTO task, int id)
        {
            return await this.PutRequest(_apiOptions.PutTaskSufix, id, task);
        }

        public async Task<string> Delete(int id)
        {
            return await this.DeleteRequest(_apiOptions.DeleteTaskSufix, id);
        }
    }
}
