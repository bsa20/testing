﻿using BSA.BL.DTO.Team;
using BSA.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetAll()
        {
            return Ok(_teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return Ok(_teamService.GetTeamById(id));
        }

        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] NewTeamDTO team)
        {
            return Ok(_teamService.AddTeam(team));
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] NewTeamDTO team, int id)
        {
            _teamService.UpdateTeam(team, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamService.DeleteTeam(id);
            return StatusCode(204);
        }

        [HttpGet("users/olderTen")]
        public ActionResult<IEnumerable<TeamWithUsersDTO>> GetTeamWithUsersOlderThanTen()
        {
            return Ok(_teamService.GetTeamsWithUsersOlderThanTenYears());
        }
    }
}