﻿using BSA.BL.DTO.Task;
using BSA.BL.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetAll()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }

        [HttpPost]
        public ActionResult<TaskDTO> Post([FromBody] NewTaskDTO task)
        {
            return Ok(_taskService.AddTask(task));
        }

        [HttpPut("{id}")]
        public ActionResult Put([FromBody] NewTaskDTO task, int id)
        {
            _taskService.UpdateTask(task, id);
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return StatusCode(204);
        }
    }
}