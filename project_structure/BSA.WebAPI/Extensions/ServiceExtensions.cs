﻿using AutoMapper;
using BSA.BL.MappingProfiles;
using BSA.BL.Services;
using BSA.Repo.Context;
using BSA.Repo.UnitOfWorker;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace BSA.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }

        public static void RegisterUnitOfWorker(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BsaDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("BsaDbConnectionString")));
            services.AddScoped<UnitOfWorker>();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ProjectService>();
            services.AddScoped<TaskService>();
            services.AddScoped<TeamService>();
            services.AddScoped<UserService>();
        }
    }
}
