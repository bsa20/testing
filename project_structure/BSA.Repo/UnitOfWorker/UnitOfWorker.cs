﻿using BSA.Repo.Context;
using BSA.Repo.Repository;
using Microsoft.EntityFrameworkCore;

namespace BSA.Repo.UnitOfWorker
{
    public class UnitOfWorker
    {
        private readonly BsaDbContext _ctx;
        private ProjectRepository projectRepository;
        private TaskRepository taskRepository;
        private TeamRepository teamRepository;
        private UserRepository userRepository;

        public UnitOfWorker(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public ProjectRepository Projects
        {
            get
            {
                if (projectRepository == null) return projectRepository = new ProjectRepository(_ctx);
                return projectRepository;
            }
        }

        public TaskRepository Tasks
        {
            get
            {
                if (taskRepository == null) return taskRepository = new TaskRepository(_ctx);
                return taskRepository;
            }
        }

        public TeamRepository Teams
        {
            get
            {
                if (teamRepository == null) return teamRepository = new TeamRepository(_ctx);
                return teamRepository;
            }
        }

        public UserRepository Users
        {
            get
            {
                if (userRepository == null) return userRepository = new UserRepository(_ctx);
                return userRepository;
            }
        }

        public void Save()
        {
            _ctx.SaveChanges();
        }

        public void DetachEntity<TEntity>(TEntity entity)
        {
            _ctx.Entry(entity).State = EntityState.Detached;
        }
    }
}
