﻿using BSA.Repo.Enums;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace BSA.Repo.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime FinishedAt { get; set; }

        [Required]
        public TaskState State { get; set; }

        [Required]
        public int ProjectId { get; set; }

        [JsonIgnore]
        [Required]
        public Project Project { get; set; }

        [Required]
        public int? PerformerId { get; set; }

        [JsonIgnore]
        [Required]
        public User Performer { get; set; }
    }
}
