﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly BsaDbContext _ctx;

        public UserRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(User entity)
        {
            entity.RegisteredAt = DateTime.Now;
            _ctx.Users.Add(entity);
        }

        public IEnumerable<User> GetAll()
        {
            return _ctx.Users.Include(u => u.Team).ToList();
        }

        public User GetById(int id)
        {
            var user = _ctx.Users.FirstOrDefault(p => p.Id == id);
            if (user == null) return user;
            _ctx.Entry(user).Reference(u => u.Team).Load();
            return user;
        }

        public void Remove(int id)
        {
            _ctx.Tasks.RemoveRange(_ctx.Tasks.Where(t => t.PerformerId == id));
            _ctx.Users.Remove(GetById(id));
        }

        public void Update(User entity)
        {
            _ctx.Users.Update(entity);
        }
    }
}
