﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly BsaDbContext _ctx;

        public ProjectRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Project entity)
        {
            entity.CreatedAt = DateTime.Now;
            _ctx.Projects.Add(entity);
        }

        public IEnumerable<Project> GetAll()
        {
            return _ctx.Projects.Include(p => p.Tasks)
                                    .ThenInclude(t => t.Performer)
                                        .ThenInclude(p => p.Team)
                                .Include(p => p.Author)
                                    .ThenInclude(a => a.Team)
                                .Include(p => p.Team).ToList();
        }

        public Project GetById(int id)
        {
            var proj = _ctx.Projects.FirstOrDefault(p => p.Id == id);
            if (proj == null) return proj;
            _ctx.Entry(proj).Reference(p => p.Team).Load();
            _ctx.Entry(proj).Reference(p => p.Author).Load();
            _ctx.Entry(proj.Author).Reference(a => a.Team).Load();
            _ctx.Entry(proj).Collection(p => p.Tasks).Load();
            foreach(var task in proj.Tasks)
            {
                _ctx.Entry(task).Reference(t => t.Performer).Load();
                _ctx.Entry(task.Performer).Reference(p => p.Team).Load();
            }
            return proj;
        }

        public void Remove(int id)
        {
            _ctx.Projects.Remove(GetById(id));
        }

        public void Update(Project entity)
        {
            _ctx.Projects.Update(entity);
        }
    }
}
