﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly BsaDbContext _ctx;

        public TeamRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Team entity)
        {
            entity.CreatedAt = DateTime.Now;
            _ctx.Teams.Add(entity);
        }

        public IEnumerable<Team> GetAll()
        {
            return _ctx.Teams.ToList();
        }

        public Team GetById(int id)
        {
            return _ctx.Teams.FirstOrDefault(p => p.Id == id);
        }

        public void Remove(int id)
        {
            _ctx.Teams.Remove(GetById(id));
        }

        public void Update(Team entity)
        {
            _ctx.Teams.Update(entity);
        }
    }
}
