﻿using BSA.Repo.Context;
using BSA.Repo.Models;
using BSA.Repo.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA.Repo.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly BsaDbContext _ctx;

        public TaskRepository(BsaDbContext ctx)
        {
            _ctx = ctx;
        }

        public void Add(Task entity)
        {
            entity.CreatedAt = DateTime.Now;
            _ctx.Tasks.Add(entity);
        }

        public IEnumerable<Task> GetAll()
        {
            return _ctx.Tasks.Include(t => t.Performer)
                                .ThenInclude(u => u.Team)
                             .Include(t => t.Project)
                                .ThenInclude(p => p.Team)
                             .Include(t => t.Project)
                                .ThenInclude(p => p.Author)
                            .Include(t => t.Project)
                                .ThenInclude(p => p.Tasks)
                                    .ThenInclude(t => t.Performer)
                                        .ThenInclude(p => p.Team)
                            .ToList();
        }

        public Task GetById(int id)
        {
            var task = _ctx.Tasks.FirstOrDefault(p => p.Id == id);
            if (task == null) return task;
            _ctx.Entry(task).Reference(t => t.Performer).Load();
            _ctx.Entry(task.Performer).Reference(p => p.Team).Load();
            _ctx.Entry(task).Reference(t => t.Project).Load();
            _ctx.Entry(task.Project).Reference(p => p.Team).Load();
            _ctx.Entry(task.Project).Reference(p => p.Author).Load();
            _ctx.Entry(task.Project.Author).Reference(a => a.Team).Load();
            _ctx.Entry(task.Project).Collection(p => p.Tasks).Load();
            foreach (var tt in task.Project.Tasks) {
                if (tt.Id != task.Id)
                {
                    _ctx.Entry(tt).Reference(t => t.Performer).Load();
                    _ctx.Entry(tt.Performer).Reference(p => p.Team).Load();
                }
            }
            return task;
        }

        public void Remove(int id)
        {
            _ctx.Tasks.Remove(GetById(id));
        }

        public void Update(Task entity)
        {
            _ctx.Tasks.Update(entity);
        }
    }
}
