﻿using AutoMapper;
using BSA.BL.DTO.Task;
using BSA.Repo.Models;

namespace BSA.BL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<NewTaskDTO, Task>();
        }
    }
}
