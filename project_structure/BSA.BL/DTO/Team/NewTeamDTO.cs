﻿namespace BSA.BL.DTO.Team
{
    public class NewTeamDTO
    {
        public string? Name { get; set; }
    }
}
