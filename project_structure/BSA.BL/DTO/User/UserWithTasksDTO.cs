﻿using BSA.BL.DTO.Task;
using System.Collections.Generic;

namespace BSA.BL.DTO.User
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
